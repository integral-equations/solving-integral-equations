SHELL := /bin/bash
#CXX = g++ -std=c++11 -fopenmp
#CXX = g++ -O3 -std=c++11 -fopenmp
CXX = g++ -Wall -Wextra -Wfloat-equal -Wundef -Wlogical-op -Wmissing-declarations -Wredundant-decls -Wshadow -std=c++11 -fopenmp

DEPS = src/integral.h src/root_solver.h src/grid.h src/solving_integral_equations.h
OBJ = obj/main.o obj/integral.o obj/root_solver.o obj/grid.o obj/solving_integral_equations.o 


obj/%.o: src/%.cpp $(DEPS)
	$(CXX) -I/usr/local/include -c $< -o $@

run: $(OBJ) 
	$(CXX) -o bin/run.out $(OBJ) -L/usr/local/lib -lgsl -lgslcblas -lm

clean: 
	rm -f bin/run.out $(OBJ)

