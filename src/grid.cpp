#include <cmath>
#include <vector>
#include "grid.h"

using namespace std;


OneDimensionalGrid::OneDimensionalGrid(double min, double max, int N)
{	
	gridMin = min;
	gridMax = max;
	gridLength = N;
	gridDelta = (gridMax - gridMin)/( gridLength - 1 );
	gridPoints.assign(N, 0.0);
	for (int i = 0; i < N; ++i){ gridPoints[i] = gridMin + i*gridDelta; }
}


