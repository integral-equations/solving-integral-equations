#ifndef INTEGRAL_H
#define INTEGRAL_H

using namespace std;

double integrate_QNG(double, double, void*, double (double, void*), double);

double integrate_QNG_PRO(string , double , double , void* , double (double, void*), double , double );

double integrate_QAG(double, double, void*, double (double, void*), int, double);

double integrate_QAG_PRO(string , double , double , void* , double (double, void*), int key, int , double , double );

double integrate_QAGS(double, double, void*, double (double, void*), double);

double integrate_QAGS_PRO(string , double, double, void*, double (double, void*), int, double, double);

double integrate_CQUAD(double, double, void*, double (double, void*), double);

double integrate_QAWC(double, double, double, void*, double (double, void*), double);

double integrate_QAWC_PRO(string , double , double , double , void* , double (double, void*), int , double , double );

double integrate_QAGP(double, vector<double>, double, void*, double (double, void*), double);

double integrate_QAGP_PRO(string , double , vector<double> , double , void* , double (double, void*), int , double , double );

double integrate_QAGI_X_to_inf(double , void* , double (double, void*), double);

double integrate_QAGI_X_to_inf_PRO(string , double , void* , double (double, void*), int , double , double );

double integrate_QAWS(double , double , double , double , int , int , void* , double (double, void*), double );

double Integrate_with_composite_trapezoidal_rule(int , double , double , double (double, void*), void *);

#endif
