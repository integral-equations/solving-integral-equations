#include <cmath>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_math.h>
#include "integral.h"

using namespace std;


//QNG NON-ADAPTIVE GAUSS-KRONROD INTEGRATION
//The QNG algorithm is a non-adaptive procedure which uses fixed Gauss-Kronrod-Patterson abscissae to sample 
//the integrand at a maximum of 87 points. It is provided for fast integration of smooth functions.
//This function evaluates the bounded integral of some function placeholder_f
double integrate_QNG(double x_init, double x_final, void* params, double placeholder_f (double, void*), double precision)
{
    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;
    size_t neval;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = precision;
    const double epsrel = precision;
    
    int code = gsl_integration_qng(&F, xlow, xhigh, epsabs, epsrel, &result, &error, &neval);

    if( code!=0 ){}
    
    return result;
}
double integrate_QNG_PRO(string integral_ID, double x_init, double x_final, void* params, double placeholder_f (double, void*), double abs_precision, double rel_precision)
{
    gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off(); //save original handler, turn off the error handler

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;
    size_t neval;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = abs_precision;
    const double epsrel = rel_precision;
    
    int code = gsl_integration_qng(&F, xlow, xhigh, epsabs, epsrel, &result, &error, &neval);
    
    if( code!=0 )
    {   
        cout << "Problem in the integration using QNG: " << integral_ID << "\n";
        //integral_inspector(integral_ID, params, code);
    }

    gsl_set_error_handler(old_error_handler); //reset the error handler 

    return result;
}



//QAG ADAPTIVE INTEGRATION
//The QAG algorithm is an adaptive integrator. The integration region is divided into subintervals, and on each 
//iteration the subinterval with the largest estimated error is bisected. This reduces the overall error rapidly, 
//as the subintervals become concentrated around local difficulties in the integrand. A "key" has to be chosen.
//This function evaluates the bounded integral of some function placeholder_f   
double integrate_QAG(double x_init, double x_final, void* params, double placeholder_f (double, void*), int key, double precision)
{
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc (2000);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = precision;
    const double epsrel = precision;

    int X = key;//1,2,3,4,5,6 choose different integration rules, see GSL documentation. 
                //The higher-order rules give better accuracy for smooth functions, while 
                //lower-order rules save time when the function contains local difficulties, 
                //such as discontinuities.
    if (X<1 || X>6){ printf("Problem with key in QAG integration!\n"); }
    
    int code = gsl_integration_qag(&F, xlow, xhigh, epsabs, epsrel, 2000, X, work_ptr, &result, &error);

    if( code!=0 ){}
    
    gsl_integration_workspace_free(work_ptr);

    return result;
}
double integrate_QAG_PRO(string integral_ID, double x_init, double x_final, void* params, double placeholder_f (double, void*), int key, int workspace_size, double abs_precision, double rel_precision)
{
    gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off(); //save original handler, turn off the error handler

    const size_t integration_workspace_size = workspace_size;
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc(integration_workspace_size);


    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = abs_precision;
    const double epsrel = rel_precision;

    int X = key;//1,2,3,4,5,6 choose different integration rules, see GSL documentation. 
                //The higher-order rules give better accuracy for smooth functions, while 
                //lower-order rules save time when the function contains local difficulties, 
                //such as discontinuities.
    if (X<1 || X>6){ printf("Problem with key in QAG integration!\n"); }
    
    int code = gsl_integration_qag(&F, xlow, xhigh, epsabs, epsrel, workspace_size, X, work_ptr, &result, &error);
    
    if( code!=0 )
    {   
        cout << "Problem in the integration using QAG: " << integral_ID << "\n";
        //integral_inspector(integral_ID, params, code);
    }

    gsl_integration_workspace_free(work_ptr);

    gsl_set_error_handler(old_error_handler); //reset the error handler 

    return result;
}




//QAGS ADAPTIVE INTEGRATION WITH SINGULARITIES
//This function applies the Gauss-Kronrod 21-point integration rule adaptively. The results are extrapolated using the
//epsilon-algorithm, which accelerates the convergence of the integral in the presence of discontinuities and integrable singularities.
//The subintervals and their results are stored in the memory provided by workspace. The maximum number of subintervals is 
//given by limit, which may not exceed the allocated size of the workspace.
//This function evaluates the bounded integral of some function placeholder_f 
double integrate_QAGS(double x_init, double x_final, void* params, double placeholder_f (double, void*), double precision)
{
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc (2000);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = precision;
    const double epsrel = precision;
    
    int code = gsl_integration_qags(&F, xlow, xhigh, epsabs, epsrel, 2000, work_ptr, &result, &error);

    if( code!=0 ){}

    gsl_integration_workspace_free(work_ptr);
    
    return result;
}
double integrate_QAGS_PRO(string integral_ID, double x_init, double x_final, void* params, double placeholder_f (double, void*), int workspace_size, double abs_precision, double rel_precision)
{   
    gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off(); //save original handler, turn off the error handler

    const size_t integration_workspace_size = workspace_size;
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc(integration_workspace_size);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = abs_precision;
    const double epsrel = rel_precision;
    
    int code = gsl_integration_qags(&F, xlow, xhigh, epsabs, epsrel, integration_workspace_size, work_ptr, &result, &error);
    
    if( code!=0 )
    {   
        cout << "Problem in the integration using QAGS: " << integral_ID << "\n";
        //integral_inspector(integral_ID, params, code);
    }

    gsl_integration_workspace_free(work_ptr);

    gsl_set_error_handler(old_error_handler); //reset the error handler 
    
    return result;
}



//CQUAD DOUBLY-ADAPTIVE INTEGRATION
//CQUAD is a new doubly-adaptive general-purpose quadrature routine which can handle most types of singularities, 
//non-numerical function values such as Inf or NaN, as well as some divergent integrals. It generally requires more 
//function evaluations than the integration routines in QUADPACK, yet fails less often for difficult integrands. 
//This function evaluates the bounded integral of some function placeholder_f 
double integrate_CQUAD(double x_init, double x_final, void* params, double placeholder_f (double, void*), double precision)
{   
    //A minimum of 3 intervals is required and for most functions, a workspace of size 100 is sufficient.
    gsl_integration_cquad_workspace *work_ptr = gsl_integration_cquad_workspace_alloc (100); 

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = precision;
    const double epsrel = precision;

    //If the error estimate or the number of function evaluations is not needed, the pointers abserr and nevals can be set to NULL.
    int code = gsl_integration_cquad(&F, xlow, xhigh, epsabs, epsrel, work_ptr, &result, NULL, NULL);

    if( code!=0 ){}

    gsl_integration_cquad_workspace_free (work_ptr);
    
    return result;
}



//QAWC
//This function computes the Cauchy principal value of the integral of f over (a, b),
//with a singularity at c. The integration must be written as: int_a^b dx f(x)/(x-c) i.e. the place function defined by the 
//user is only the numerator.
double integrate_QAWC(double x_init, double x_final, double x_singular, void* params, double placeholder_f (double, void*), double precision)
{
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc (2000);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double xsing = x_singular;
    const double epsabs = precision;
    const double epsrel = precision;
    
    int code = gsl_integration_qawc(&F, xlow, xhigh, xsing, epsabs, epsrel, 2000, work_ptr, &result, &error);

    if( code!=0 ){}

    gsl_integration_workspace_free(work_ptr);
    
    return result;
}
double integrate_QAWC_PRO(string integral_ID, double x_init, double x_final, double x_singular, void* params, double placeholder_f (double, void*), int workspace_size, double abs_precision, double rel_precision)
{
    gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off(); //save original handler, turn off the error handler

    const size_t integration_workspace_size = workspace_size;
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc(integration_workspace_size);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double xsing = x_singular;
    const double epsabs = abs_precision;
    const double epsrel = rel_precision;
    
    int code = gsl_integration_qawc(&F, xlow, xhigh, xsing, epsabs, epsrel, integration_workspace_size, work_ptr, &result, &error);

    if( code!=0 )
    {   
        cout << "Problem in the integration using QAWC: " << integral_ID << "\n";
        //integral_inspector(integral_ID, params, code);
    }

    gsl_integration_workspace_free(work_ptr);

    gsl_set_error_handler(old_error_handler); //reset the error handler 
    
    return result;
}



//QAGP
//This function applies the adaptive integration algorithm QAGS taking account of the user-supplied locations of singular points. 
//The array pts of length npts should contain the endpoints of the integration ranges deﬁned by the integration region and locations
//of the singularities.
double integrate_QAGP(double x_init, vector<double> singular_points, double x_final, void* params, double placeholder_f (double, void*), double precision)
{
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc (1000);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const vector<double> xsing = singular_points;

    const double epsabs = precision;
    const double epsrel = precision;

    vector<double> x;

    x.push_back(xlow);
    const int number_singular_points = singular_points.size();
    for (int i=0; i<number_singular_points; i++)
    {
        x.push_back(singular_points[i]);
    }
    x.push_back(xhigh);
    sort(x.begin(), x.end());

    const size_t xsize=2+singular_points.size();

    int code = gsl_integration_qagp (&F, &x[0], xsize, epsabs, epsrel, 1000, work_ptr,  &result, &error);

    if( code!=0 ){}

    gsl_integration_workspace_free(work_ptr);
    
    return result;
}
double integrate_QAGP_PRO(string integral_ID, double x_init, vector<double> singular_points, double x_final, void* params, double placeholder_f (double, void*), int workspace_size, double abs_precision, double rel_precision)
{
    gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off(); //save original handler, turn off the error handler

    const size_t integration_workspace_size = workspace_size;
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc(integration_workspace_size);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const vector<double> xsing = singular_points;
    const double epsabs = abs_precision;
    const double epsrel = rel_precision;

    vector<double> x;

    x.push_back(xlow);
    const int number_singular_points = singular_points.size();
    for (int i=0; i<number_singular_points; i++)
    {
        x.push_back(singular_points[i]);
    }
    x.push_back(xhigh);
    sort(x.begin(), x.end());

    const size_t xsize = 2 + singular_points.size();

    int code = gsl_integration_qagp (&F, &x[0], xsize, epsabs, epsrel, 1000, work_ptr,  &result, &error);

    if( code!=0 )
    {   
        cout << "Problem in the integration using QAGP: " << integral_ID << "\n";
        //integral_inspector(integral_ID, params, code);
    }

    gsl_integration_workspace_free(work_ptr);

    gsl_set_error_handler(old_error_handler); //reset the error handler 
    
    return result;
}


//QAGI
//This function computes the integral of the function f over the semi-inﬁnite interval
double integrate_QAGI_X_to_inf(double x_init, void* params, double placeholder_f (double, void*), double precision)
{
  gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc (1000);

  gsl_function F;
  F.function = placeholder_f;
  F.params = params;

  double result, error;

  const double xlow = x_init;
  const double epsabs = precision;
  const double epsrel = precision;
  
  int code = gsl_integration_qagiu(&F, xlow, epsabs, epsrel, 1000, work_ptr, &result, &error);

  if( code!=0 ){}

  gsl_integration_workspace_free(work_ptr);
  
  return result;
}
double integrate_QAGI_X_to_inf_PRO(string integral_ID, double x_init, void* params, double placeholder_f (double, void*), int workspace_size, double abs_precision, double rel_precision)
{   
     gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off(); //save original handler, turn off the error handler

    const size_t integration_workspace_size = workspace_size;
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc(integration_workspace_size);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double epsabs = abs_precision;
    const double epsrel = rel_precision;
  
    int code = gsl_integration_qagiu(&F, xlow, epsabs, epsrel, integration_workspace_size, work_ptr, &result, &error);

    if( code!=0 )
    {   
        cout << "Problem in the integration using QAGI_X_to_inf: " << integral_ID << "\n";
        //integral_inspector(integral_ID, params, code);
    }

    gsl_integration_workspace_free(work_ptr);

    gsl_set_error_handler(old_error_handler); //reset the error handler 
  
  return result;
}


//QAWS
double integrate_QAWS(double x_init, double x_final, double alpha, double beta, int mu, int nu, void* params, double placeholder_f (double, void*), double precision)
{
    gsl_integration_workspace *work_ptr = gsl_integration_workspace_alloc (2000);

    gsl_integration_qaws_table *table = gsl_integration_qaws_table_alloc(alpha, beta , mu, nu);

    gsl_function F;
    F.function = placeholder_f;
    F.params = params;

    double result, error;

    const double xlow = x_init;
    const double xhigh = x_final;
    const double epsabs = precision;
    const double epsrel = precision;
    
    int code = gsl_integration_qaws (&F, xlow, xhigh, table, epsabs, epsrel, 2000, work_ptr, &result, &error);

    if( code!=0 ){}

    gsl_integration_workspace_free(work_ptr);

    gsl_integration_qaws_table_free(table);
    
    return result;
}


//Integrate using the composite trapezoidal rule
double Integrate_with_composite_trapezoidal_rule(int N, double min, double max, double function(double, void*), void *parameters)
{
    double dx = (max-min)/(N-1);
    double area = 0.0;
    for (int i = 1; i < N-1; ++i)
    {   
        area = area + function(min + i*dx, parameters);
    }

    area = dx*( area + 0.5*function(min, parameters) + 0.5*function(max, parameters) );

    return area;
}
