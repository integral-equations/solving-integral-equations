#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <gsl/gsl_chebyshev.h>
#include "integral.h"
#include "root_solver.h"
#include "grid.h"
#include "solving_integral_equations.h"

using namespace std;


int gsl_vector_size(const gsl_vector * x)
{
    int size = x->size;
    return size;
}


ChebyshevPolynomialsFirstKind::ChebyshevPolynomialsFirstKind(vector<double> coefficientsAux)
{
	coefficients = coefficientsAux;
	order = coefficients.size();
}


double ChebyshevPolynomialsFirstKindRecursionRelation(int n, double x)
{	
	double T = 0.0;

	if ( n==0 )
	{ 
		T = 1.0; 
	}
	else
	{
		if ( n==1 )
		{ 
			T = x; 
		}
		else{        
			T = 2*x*ChebyshevPolynomialsFirstKindRecursionRelation(n-1, x) - ChebyshevPolynomialsFirstKindRecursionRelation(n-2, x); 
		}
	}

	return T;
}


double ChebyshevPolynomialsFirstKind::evaluate(double x)
{	
	double f = -0.5*coefficients[0];
	for (int i = 0; i < order; ++i)
	{
		f = f + coefficients[i]*ChebyshevPolynomialsFirstKindRecursionRelation(i, x);
	}

	return f;
}


int solveIntegralEquationByCollocation(const gsl_vector *X, void *parameters, gsl_vector *F)
{		
	const int N = gsl_vector_size(X);


	vector<double> coeff(N, 0.0);
	for (int i = 0; i < N; ++i)
	{ 
		coeff[i] = gsl_vector_get(X,i); 
	}

	//get parameters: xmin and xmax used in the discretization and integral equation
	double xMin = ((struct integralEquationParameters *)(parameters))->xMin;
	double xMax = ((struct integralEquationParameters *)(parameters))->xMax;
	IntegralEquationWithOneFunctionExpansion equation = ((struct integralEquationParameters *)(parameters))->function;
	
	//create one dimensional grid for collocation method
	OneDimensionalGrid grid(xMin, xMax, N);


	//equations to solve discretized in x
	vector<double> fAux(N,0);
	for (int i = 0; i < N; ++i)
	{	
		double xi = grid.getGridPoint(i);
		fAux[i] = equation.evaluate(xi, coeff);
	}

	
	for (int i = 0; i < N; ++i){ gsl_vector_set(F,i,fAux[i]); }


	return GSL_SUCCESS;
}










////////////////////////////////////////////////////////////////////////////////////////


double f(double x, void *p)
{
	(void)(p); /* avoid unused parameter warning */

	if (x < 0.5)
	{
		return 0.25;
	}
	else
	{
		return 0.75;
	}
}


void test()
{
	int n = 10000;

	const int Ntruncation = 80;
	gsl_cheb_series *cs = gsl_cheb_alloc (Ntruncation);

	gsl_function F;

	F.function = f;
	F.params = 0;

	gsl_cheb_init (cs, &F, 0.0, 1.0);

	vector<double> xAux(n,0);
	vector<double> r10Aux(n,0);
	vector<double> r40Aux(n,0);
	vector<double> fTrue(n,0);

	for (int i = 0; i < n; i++)
	{
		double x = i / (double)n;
		double r10 = gsl_cheb_eval_n (cs, 10, x);
		double r40 = gsl_cheb_eval (cs, x);
		printf ("%g %g %g %g\n", x, GSL_FN_EVAL (&F, x), r10, r40);

		xAux[i] = x;
		fTrue[i] = f(x, 0);
		r10Aux[i] = r10;
		r40Aux[i] = r40;
	}

	gsl_cheb_free (cs);

	
	std::ofstream file;
    file.open("test.dat", std::ofstream::out | std::ios::trunc);
    file.precision(15);
    for (int i = 0; i < n; ++i)
    {   
        file.width(25);   file << xAux[i];
        file.width(25);   file << fTrue[i];
        file.width(25);   file << r10Aux[i];
        file.width(25);   file << r40Aux[i];
        file << "\n";
    }
    file.close();

}



