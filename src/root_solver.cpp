#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>
#include "root_solver.h"

using namespace std;


//FUNCTION THAT CALCULATE MULTI-ROOTS
void find_root (int n_eqs, double precision, double* x_init, void* params, int placeholder_f(const gsl_vector*, void*, gsl_vector*), int method)
{
	//SET NUMBER OF EQUATIONS
	const size_t dim = n_eqs;

	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;
	

	//CHOOSE ROOT FINDING METHOD
	if ( method==1 )
	{
		T = gsl_multiroot_fsolver_hybrids;
	}
	else if ( method==2 )
	{
		T = gsl_multiroot_fsolver_hybrid;
	}
	else if ( method==3 )
	{
		T = gsl_multiroot_fsolver_dnewton;
	}
	else if ( method==4 )
	{
		T = gsl_multiroot_fsolver_broyden;
	}
	else
	{
		cout << "\nNO MODEL UNDER THAT INDEX, choosing dnewton\n";
		T = gsl_multiroot_fsolver_dnewton;
	}

	//ALLOCATE MEMORY
	s = gsl_multiroot_fsolver_alloc(T,dim);		

	gsl_vector *x = gsl_vector_alloc(dim);

	//SET INITIAL GUESS
	for (int i = 0; i < n_eqs; ++i) gsl_vector_set(x,i,x_init[i]);	
	
	//SET SYSTEM OF EQUATIONS THAT WILL ENTER IN THE METHOD
	gsl_multiroot_function f = {placeholder_f, dim, params};

	//SET UP METHOD
	gsl_multiroot_fsolver_set (s, &f, x);

	//START ITERATION
	int counter = 0;
	int status = GSL_CONTINUE;

	while (status == GSL_CONTINUE && counter < 1000){

		++counter;
		status = gsl_multiroot_fsolver_iterate(s);

		if (status) break;

		status = gsl_multiroot_test_residual (s->f, precision);
	}

	//SET SOLUTIONS TO INITIAL VECTOR
	for (int i = 0; i < n_eqs; ++i) x_init[i] = gsl_vector_get(s->x, i);

	//FREE MEMORY
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);
}


int find_root_PRO (int n_eqs, double precision, double* x_init, void* params, int placeholder_f(const gsl_vector*, void*, gsl_vector*), int method){
	//SET NUMBER OF EQUATIONS
	const size_t dim = n_eqs;

	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;
	

	//CHOOSE ROOT FINDING METHOD
	if ( method==1 )
	{
		T = gsl_multiroot_fsolver_hybrids;
	}
	else if ( method==2 )
	{
		T = gsl_multiroot_fsolver_hybrid;
	}
	else if ( method==3 )
	{
		T = gsl_multiroot_fsolver_dnewton;
	}
	else if ( method==4 )
	{
		T = gsl_multiroot_fsolver_broyden;
	}
	else
	{
		cout << "\nNO MODEL UNDER THAT INDEX, choosing dnewton\n";
		T = gsl_multiroot_fsolver_dnewton;
	}

	//ALLOCATE MEMORY
	s = gsl_multiroot_fsolver_alloc(T,dim);		

	gsl_vector *x = gsl_vector_alloc(dim);

	//SET INITIAL GUESS
	for (int i = 0; i < n_eqs; ++i) gsl_vector_set(x,i,x_init[i]);	
	
	//SET SYSTEM OF EQUATIONS THAT WILL ENTER IN THE METHOD
	gsl_multiroot_function f = {placeholder_f, dim, params};

	//SET UP METHOD
	gsl_multiroot_fsolver_set (s, &f, x);

	//START ITERATION
	int counter = 0;
	int status = GSL_CONTINUE;

	while (status == GSL_CONTINUE && counter < 1000){

		++counter;
		status = gsl_multiroot_fsolver_iterate(s);

		if (status) break;

		status = gsl_multiroot_test_residual (s->f, precision);
	}

	//SET SOLUTIONS TO INITIAL VECTOR
	for (int i = 0; i < n_eqs; ++i) x_init[i] = gsl_vector_get(s->x, i);

	//FREE MEMORY
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);

	if (status==0){ return 0; }
	else{ return -1; }
}


double one_dimension_root_finding(double precision, double x_low, double x_high, void* params, double placeholder_f (double, void*), int method)
{	
	gsl_function F;
    F.function = placeholder_f;
    F.params = params;

	const gsl_root_fsolver_type *T;
	gsl_root_fsolver *s;
	
	//CHOOSE ROOT FINDING METHOD
	if ( method==1 )
	{
		T = gsl_root_fsolver_brent;
	}
	else if ( method==2 )
	{
		T = gsl_root_fsolver_bisection;
	}
	else if ( method==3 )
	{
		T = gsl_root_fsolver_falsepos;
	}
	else
	{
		cout << "\nNO MODEL UNDER THAT INDEX, choosing brent\n";
		T = gsl_root_fsolver_brent;
	}

	//ALLOCATE MEMORY
	s =  gsl_root_fsolver_alloc (T);		

	gsl_root_fsolver_set (s, &F, x_low, x_high);


	//START ITERATION
	int counter = 0;
	int status = GSL_CONTINUE;

	double root;
	while (status == GSL_CONTINUE && counter < 1000){

		++counter;
		status = gsl_root_fsolver_iterate(s);

		root = gsl_root_fsolver_root (s);
		x_low = gsl_root_fsolver_x_lower (s);
		x_high = gsl_root_fsolver_x_upper (s);

		if (status) break;

		status = gsl_root_test_interval (x_low, x_high, 0, precision);
	}


	//FREE MEMORY
	gsl_root_fsolver_free (s);

	return root;
}

/*
////////////////////////////////////////////////////////////////////////////////////////
//cubic equation solver using Cardano's method: a*x^3 + b*x^2 + c*x + d = 0


//sort a tuple of 3 elements (complex numbers) by absolute value
tuple<gsl_complex, gsl_complex, gsl_complex> sort_complex_tuple_size_3(tuple<gsl_complex, gsl_complex, gsl_complex> set)
{	
	vector<gsl_complex> non_ordered_set;
	non_ordered_set.push_back( get<0>(set) );
	non_ordered_set.push_back( get<1>(set) );
	non_ordered_set.push_back( get<2>(set) );

	double ABS[3];
	ABS[0] = gsl_complex_abs( non_ordered_set[0] );
	ABS[1] = gsl_complex_abs( non_ordered_set[1] );
	ABS[2] = gsl_complex_abs( non_ordered_set[2] );

	vector<tuple<double, int>> aux;
	aux.push_back( make_tuple(ABS[0], 0) );
	aux.push_back( make_tuple(ABS[1], 1) );
	aux.push_back( make_tuple(ABS[2], 2) );
	sort(aux.begin(), aux.end());

	int index_0 = get<1>(aux[0]);
	int index_1 = get<1>(aux[1]);
	int index_2 = get<1>(aux[2]);

	tuple<gsl_complex, gsl_complex, gsl_complex> ordered_set = make_tuple( non_ordered_set[index_0], non_ordered_set[index_1], non_ordered_set[index_2] );

	return ordered_set;
}


// term1_sqrt = -((b^2 c^2)/a^4), term2_sqrt = +((4 c^3)/a^3), term3_sqrt = +((4 b^3 d)/a^4), term4_sqrt = -((18 b c d)/a^3), term5_sqrt = +((27 d^2)/a^2)
// term1 = -((2 b^3)/a^3), term2 = +((9 b c)/a^2), term3 = -((27 d)/a)
gsl_complex cardano_A(gsl_complex a, gsl_complex b, gsl_complex c, gsl_complex d)
{   
    gsl_complex a2 = gsl_complex_pow_real(a, 2.0);   
    gsl_complex b2 = gsl_complex_pow_real(b, 2.0);
    gsl_complex c2 = gsl_complex_pow_real(c, 2.0);
    gsl_complex d2 = gsl_complex_pow_real(d, 2.0);
    gsl_complex a3 = gsl_complex_pow_real(a, 3.0);
    gsl_complex b3 = gsl_complex_pow_real(b, 3.0);
    gsl_complex c3 = gsl_complex_pow_real(c, 3.0);
    gsl_complex a4 = gsl_complex_pow_real(a, 4.0);
    gsl_complex bc = gsl_complex_mul(b, c);
    gsl_complex b2_c2 = gsl_complex_mul(b2, c2);
    gsl_complex b3d = gsl_complex_mul(b3, d);
    gsl_complex bcd = gsl_complex_mul(bc, d);


    //calculating terms inside the square root
    gsl_complex term1_sqrt = gsl_complex_mul_real(gsl_complex_div(b2_c2, a4), -1);
    gsl_complex term2_sqrt = gsl_complex_mul_real(gsl_complex_div(c3, a3), +4);
    gsl_complex term3_sqrt = gsl_complex_mul_real(gsl_complex_div(b3d, a4), +4);
    gsl_complex term4_sqrt = gsl_complex_mul_real(gsl_complex_div(bcd, a3), -18);
    gsl_complex term5_sqrt = gsl_complex_mul_real(gsl_complex_div(d2, a2), +27);


    //calculating the square root
    gsl_complex A_sqrt = gsl_complex_rect(0.0, 0.0);
    A_sqrt = gsl_complex_add(A_sqrt, term1_sqrt);
    A_sqrt = gsl_complex_add(A_sqrt, term2_sqrt);
    A_sqrt = gsl_complex_add(A_sqrt, term3_sqrt);
    A_sqrt = gsl_complex_add(A_sqrt, term4_sqrt);
    A_sqrt = gsl_complex_add(A_sqrt, term5_sqrt);
    A_sqrt = gsl_complex_sqrt(A_sqrt);

    gsl_complex term1 = gsl_complex_mul_real(gsl_complex_div(b3, a3), -2);
    gsl_complex term2 = gsl_complex_mul_real(gsl_complex_div(bc, a2), +9);
    gsl_complex term3 = gsl_complex_mul_real(gsl_complex_div(d, a), -27);


    gsl_complex A_aux = gsl_complex_rect(0.0, 0.0);
    A_aux = gsl_complex_add(A_aux, term1);
    A_aux = gsl_complex_add(A_aux, term2);
    A_aux = gsl_complex_add(A_aux, term3);
    A_aux = gsl_complex_add(A_aux, gsl_complex_mul_real(A_sqrt, 3.0*sqrt(3.0)) );
    A_aux = gsl_complex_pow_real(A_aux, 1./3);
    A_aux = gsl_complex_mul_real(A_aux, 1./(3.0*pow(2, 1./3)));

    return A_aux;
}


//B = (-9 a b^2 + 27 a^2 c)/(27 a^3)
gsl_complex cardano_B(gsl_complex a, gsl_complex b, gsl_complex c, gsl_complex d)
{   
    gsl_complex b2 = gsl_complex_pow_real(b, 2.0);
    gsl_complex ab2 = gsl_complex_mul(a, b2);
    gsl_complex a2 = gsl_complex_pow_real(a, 2.0);  
    gsl_complex a2c = gsl_complex_mul(a2, c);
    gsl_complex a3 = gsl_complex_pow_real(a, 3.0);

    gsl_complex B_aux = gsl_complex_rect(0.0, 0.0);
    B_aux = gsl_complex_mul_real(ab2, -9);
    B_aux = gsl_complex_add(B_aux, gsl_complex_mul_real(a2c, +27));
    B_aux = gsl_complex_mul_real(gsl_complex_div(B_aux, a3), 1./27);

    return B_aux;
}


tuple<gsl_complex, gsl_complex, gsl_complex> solve_cubic_equation_CARDANO(gsl_complex a, gsl_complex b, gsl_complex c, gsl_complex d)
{   
    gsl_complex A = cardano_A(a, b, c, d);
    gsl_complex B = cardano_B(a, b, c, d);
    gsl_complex phi_m = gsl_complex_polar(1.0, -2.0*M_PI/3.0);
    gsl_complex phi_p = gsl_complex_polar(1.0, +2.0*M_PI/3.0);

    gsl_complex x1 = gsl_complex_rect(0.0, 0.0);
    x1 = gsl_complex_add(x1, gsl_complex_mul_real(gsl_complex_div(b, a), -1./3));
    x1 = gsl_complex_add(x1, A);
    x1 = gsl_complex_add(x1, gsl_complex_mul_real(gsl_complex_div(B, A), -1./3));

    gsl_complex x2 = gsl_complex_rect(0.0, 0.0);
    x2 = gsl_complex_add(x2, gsl_complex_mul_real(gsl_complex_div(b, a), -1./3));
    x2 = gsl_complex_add(x2, gsl_complex_mul(A,phi_m));
    x2 = gsl_complex_add(x2, gsl_complex_mul_real(gsl_complex_div(B, gsl_complex_mul(A,phi_m)), -1./3));

    gsl_complex x3 = gsl_complex_rect(0.0, 0.0);
    x3 = gsl_complex_add(x3, gsl_complex_mul_real(gsl_complex_div(b, a), -1./3));
    x3 = gsl_complex_add(x3, gsl_complex_mul(A,phi_p));
    x3 = gsl_complex_add(x3, gsl_complex_mul_real(gsl_complex_div(B, gsl_complex_mul(A,phi_p)), -1./3));

    tuple<gsl_complex, gsl_complex, gsl_complex> solutions = make_tuple( x1, x2, x3 );
    solutions = sort_complex_tuple_size_3(solutions);//sort solutions by magnitude

    return solutions;
}


tuple<gsl_complex, gsl_complex, gsl_complex> calculate_eigenvalues_general_3by3_matrix(gsl_matrix_complex* M)
{	
	//write polynomial
	gsl_complex a = gsl_complex_rect(-1.0, 0.0);

    gsl_complex b = gsl_complex_rect(0.0, 0.0);
    b = gsl_complex_add(b, gsl_matrix_complex_get(M, 0, 0));
    b = gsl_complex_add(b, gsl_matrix_complex_get(M, 1, 1));
    b = gsl_complex_add(b, gsl_matrix_complex_get(M, 2, 2));
    
    gsl_complex c = gsl_complex_rect(0.0, 0.0);
    c = gsl_complex_add(c, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 1) , gsl_matrix_complex_get(M, 1, 0) ));
    c = gsl_complex_sub(c, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 0) , gsl_matrix_complex_get(M, 1, 1) ));
    c = gsl_complex_add(c, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 2) , gsl_matrix_complex_get(M, 2, 0) ));
    c = gsl_complex_add(c, gsl_complex_mul( gsl_matrix_complex_get(M, 1, 2) , gsl_matrix_complex_get(M, 2, 1) ));
    c = gsl_complex_sub(c, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 0) , gsl_matrix_complex_get(M, 2, 2) ));
    c = gsl_complex_sub(c, gsl_complex_mul( gsl_matrix_complex_get(M, 1, 1) , gsl_matrix_complex_get(M, 2, 2) ));

    gsl_complex d = gsl_complex_rect(0.0, 0.0);
    d = gsl_complex_sub(d, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 2) , gsl_complex_mul( gsl_matrix_complex_get(M, 2, 0) , gsl_matrix_complex_get(M, 1, 1) ) ));
    d = gsl_complex_add(d, gsl_complex_mul( gsl_matrix_complex_get(M, 2, 0) , gsl_complex_mul( gsl_matrix_complex_get(M, 0, 1) , gsl_matrix_complex_get(M, 1, 2) ) ));
    d = gsl_complex_add(d, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 2) , gsl_complex_mul( gsl_matrix_complex_get(M, 2, 1) , gsl_matrix_complex_get(M, 1, 0) ) ));
    d = gsl_complex_sub(d, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 0) , gsl_complex_mul( gsl_matrix_complex_get(M, 1, 2) , gsl_matrix_complex_get(M, 2, 1) ) ));
    d = gsl_complex_sub(d, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 1) , gsl_complex_mul( gsl_matrix_complex_get(M, 1, 0) , gsl_matrix_complex_get(M, 2, 2) ) ));
    d = gsl_complex_add(d, gsl_complex_mul( gsl_matrix_complex_get(M, 0, 0) , gsl_complex_mul( gsl_matrix_complex_get(M, 1, 1) , gsl_matrix_complex_get(M, 2, 2) ) ));


    tuple<gsl_complex, gsl_complex, gsl_complex> eigen = solve_cubic_equation_CARDANO(a, b, c, d);

    return eigen;
}
*/