//Renan Câmara Pereira 2022

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <tuple>
#include <omp.h>
#include <gsl/gsl_chebyshev.h>
#include <algorithm>
#include "root_solver.h"
#include "integral.h"
#include "grid.h"
#include "solving_integral_equations.h"

using namespace std;


struct kernelParameters
{
	vector<double> coefficients;
	double variable;
};


double testFunctionIntegrand(double x, void *parameters)
{   
	vector<double> c = ((struct kernelParameters *)(parameters))->coefficients;
	double y = ((struct kernelParameters *)(parameters))->variable;

	ChebyshevPolynomialsFirstKind f(c);

    double aux = ( 1.0/( 1.0 + pow(y-x,2) ) )*f.evaluate(x);

    return aux;
}


double testFunction(double x, vector<double> &coeff, void * parameters)
{
	kernelParameters K;
	K.coefficients = coeff;
	K.variable = x;
    double integral = integrate_QAGS_PRO("teste", -1, 1, &K, &testFunctionIntegrand, 2000, 1E-8, 1E-8);

    ChebyshevPolynomialsFirstKind func(coeff);

	double f = func.evaluate(x) + integral - 1.0;

	return f;
}


int main(void)
{	

	IntegralEquationWithOneFunctionExpansion func(&testFunction, NULL);

	integralEquationParameters params;
	params.xMin = 0.0;
	params.xMax = 1.0;
	params.function = func;

	const int dim = 5;
	vector<double> guesses(dim, 1.0);
	find_root(dim, 1E-10, &guesses[0], &params, &solveIntegralEquationByCollocation, 2);

	for (int i = 0; i < dim; ++i)
	{
		cout << guesses[i] << "\n";
	}
	cout << "\n";


	int N = 20;
	OneDimensionalGrid grid(params.xMin, params.xMax, N);
	for (int i = 0; i < N; ++i)
	{	
		double x = grid.getGridPoint(i);
		cout << x << "\t" << testFunction(x, guesses, NULL) << "\n";
	}





	return 0;
}



