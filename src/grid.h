#ifndef GRID_H
#define GRID_H

using namespace std;


class OneDimensionalGrid
{	
private:
	double gridMin = 0;
	double gridMax = 0;
	int gridLength = 0;
	double gridDelta = 0;
	vector<double> gridPoints;

public:
	OneDimensionalGrid(){};
	OneDimensionalGrid(double, double, int);

	double getGridMin(){ return gridMin; }
	double getGridMax(){ return gridMax; }
	int getGridLength(){ return gridLength; }
	double getGridDelta(){ return gridDelta; }
	double getGridPoint(int i){ return gridPoints[i]; }
	vector<double> getGridPoints(){ return gridPoints; }
};



#endif