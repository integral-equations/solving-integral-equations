#ifndef SOLVING_INTEGRAL_EQUATIONS_H
#define SOLVING_INTEGRAL_EQUATIONS_H

using namespace std;


int gsl_vector_size(const gsl_vector * );


class ChebyshevPolynomialsFirstKind
{
private:
	vector<double> coefficients;
	int order;
public:
	ChebyshevPolynomialsFirstKind(vector<double> );
	int getOrder(){ return order; }
	double evaluate(double );
};


double ChebyshevPolynomialsFirstKindRecursionRelation(int , double );


class OneVariableFunction
{
private:
	double (* function)(double, void * parameters);
	void * parameters;

public:
	OneVariableFunction(){};
	OneVariableFunction(double (* functionAux)(double, void * ), void * parametersAux){ function = functionAux; parameters = parametersAux; }
	void setFunction(double (* functionAux)(double, void * )){ function = functionAux; };
	void setParameters(void * parametersAux){ parameters = parametersAux; };
	double evaluate(double x){ return function(x, parameters); }
};


class IntegralEquationWithOneFunctionExpansion
{
private:
	double (* function)(double, vector<double> &, void * parameters);
	void * parameters;

public:
	IntegralEquationWithOneFunctionExpansion(){};
	IntegralEquationWithOneFunctionExpansion(double (* functionAux)(double, vector<double> &, void * ), void * parametersAux){ function = functionAux; parameters = parametersAux; }
	void setFunction(double (* functionAux)(double, vector<double> &, void * )){ function = functionAux; };
	void setParameters(void * parametersAux){ parameters = parametersAux; };
	double evaluate(double x, vector<double> &aux){ return function(x, aux, parameters); }
};


struct integralEquationParameters
{	
	double xMin;
	double xMax;
	IntegralEquationWithOneFunctionExpansion function;
};


int solveIntegralEquationByCollocation(const gsl_vector *, void *, gsl_vector *);



////////////////////////////////////////////////////////////////////////////////////////

double f(double , void *);

void test();

#endif