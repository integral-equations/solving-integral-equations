#ifndef ROOT_SOLVER_H
#define ROOT_SOLVER_H

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

void find_root (int, double, double*, void*, int (const gsl_vector*, void*, gsl_vector*), int);

int find_root_PRO (int, double, double*, void*, int (const gsl_vector*, void*, gsl_vector*), int);

double one_dimension_root_finding (double , double , double , void* , double placeholder_f (double, void*), int );

/*
tuple<gsl_complex, gsl_complex, gsl_complex> sort_complex_tuple_size_3(tuple<gsl_complex, gsl_complex, gsl_complex> );

gsl_complex cardano_A(gsl_complex , gsl_complex , gsl_complex , gsl_complex );

gsl_complex cardano_B(gsl_complex , gsl_complex , gsl_complex , gsl_complex );

tuple<gsl_complex, gsl_complex, gsl_complex> solve_cubic_equation_CARDANO(gsl_complex , gsl_complex , gsl_complex , gsl_complex );

tuple<gsl_complex, gsl_complex, gsl_complex> calculate_eigenvalues_general_3by3_matrix(gsl_matrix_complex* );
*/

#endif